package au.com.ticgroup.create.receipt.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import au.com.ticgroup.soap.TicReceiptService.TicReceiptService_Service;

@Configuration
public class BeanConfiguration {
	
	@Bean
	public TicReceiptService_Service getTicReceiptServiceSOAP() {
		return new TicReceiptService_Service();
	}
	
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
		
}

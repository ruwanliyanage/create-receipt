package au.com.ticgroup.create.receipt.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.datatype.XMLGregorianCalendar;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@XmlRootElement(name = "TGGCRCReceiptDesc")
@XmlAccessorType(XmlAccessType.NONE)
public class TggRequestDto {

	@XmlElement(name = "transfer_number")
	String transferNumber;
	
	@XmlElement(name = "store_id")
	String storeId;
	
	@XmlElement(name = "source_id")
	String sourceId;
	
	@XmlElement(name = "user")
	String user;
	
	@XmlElement(name = "receipt_date")
	XMLGregorianCalendar receiptDate;
	
	@XmlElement(name = "status")
	String status;	

	@XmlElement(name = "TGGCRCReceiptItem")
	List<TggCrcReceiptItem> tggCrcReceiptItem;

}

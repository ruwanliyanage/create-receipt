package au.com.ticgroup.create.receipt.util;

public interface HsConstants {
	static final String SUCCESS_RESPONSE = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Header/><soapenv:Body/></soapenv:Envelope>";
	static final String ERROR_RESPONSE = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Body><SOAP:Fault><faultcode>Server-error</faultcode><faultstring>host: temporary name resolution failure %1$s </faultstring></SOAP:Fault></soapenv:Body></soapenv:Envelope>";
	static final String HS_SOAP_ENVELOPE_HSCLAIMSERVICE_START = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:nam=\"http://www.nrf-arts.org/IXRetail/namespace/\">";
	static final String HS_SOAPHEADER_START = "<soapenv:Header>";
	static final String HS_SOAPHEADER_END = "</soapenv:Header>";
	static final String HS_SECURITY_START = "<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">";
	static final String HS_SECURITY_END = "</wsse:Security>";
	static final String HS_TOKEN_START = "<wsse:UsernameToken wsu:Id=\"UsernameToken-3\">";
	static final String HS_TOKEN_END = "</wsse:UsernameToken>";
	static final String HS_SOAP_BODY_START = "<soapenv:Body>";
	static final String HS_SOAP_BODY_END = "</soapenv:Body>";
	static final String HS_SOAP_ENVELOPE_END = "</soapenv:Envelope>";
	static final String HS_SOAP_BLANK_RESPONSE = "<SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'><SOAP:Header/><SOAP:Body/></SOAP:Envelope>";
	static final String TIC_SOAP_CLAIMS_RESPONSE_OK = "<POSLogResponse><Status>OK</Status></POSLogResponse>";
	static final String TIC_SOAP_LOGISTICS_RESPONSE_OK = "<InventoryResponse><Status>OK</Status></InventoryResponse>";
}

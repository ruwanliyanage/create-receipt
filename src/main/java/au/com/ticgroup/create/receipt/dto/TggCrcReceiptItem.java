package au.com.ticgroup.create.receipt.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@XmlRootElement(name = "TGGCRCReceiptItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class TggCrcReceiptItem {
	
	String sku;
	
    String barcode;
	
    int quantity;
	
	@XmlElement(name = "credit_qty")
    int creditQuanity;
	
	@XmlElement(name = "damaged_qty")
    int damagedQuanity;

	@XmlElement(name = "TGGCRCReceiptCases")
    TggCrcReceiptCase receiptCase;

}

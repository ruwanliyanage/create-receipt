package au.com.ticgroup.create.receipt.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "hs.config")
@Getter
@Setter
public class HsConfig {
	
	private String logisticsUrl;
	
	private String logisticsAction;
	
	private String outBoundSecurity;
	
	private String username;
	
	private String password;

}

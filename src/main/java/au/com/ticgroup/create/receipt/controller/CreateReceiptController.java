package au.com.ticgroup.create.receipt.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;
import au.com.ticgroup.create.receipt.dto.TggRequestDto;
import au.com.ticgroup.create.receipt.service.CreateReceiptService;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CreateReceiptController {

	@Autowired
	private CreateReceiptService receiptService;

	@PostMapping(path = "/tgg", produces = "application/xml")
	public String createTggReceipt(@RequestBody TggRequestDto requestObject) throws UnsupportedEncodingException,
			ParserConfigurationException, SAXException, IOException, JAXBException {
		log.info("Invoking CreateReceiptController :: createTggReceipt() :: body ::" + requestObject);
		return receiptService.createTggReceipt(requestObject);
	}

	@PostMapping(path = "/hsf", produces = "application/xml")
	public String createHsfReceipt(@RequestBody String xml) throws UnsupportedEncodingException, ParserConfigurationException,
			SAXException, IOException, JAXBException {
		log.info("Invoking CreateReceiptController :: createHsfReceipt() :: body ::" + xml);
		return receiptService.createHsfReceipt(xml);
	}

}

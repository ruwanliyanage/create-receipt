package au.com.ticgroup.create.receipt.util;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpUtil {
	
	public static Response makeXmlPostRequest(String url, RequestBody body, String action, String username,
			String password) throws IOException {
		OkHttpClient client = new OkHttpClient.Builder().connectTimeout(15, TimeUnit.MINUTES)
				.readTimeout(15, TimeUnit.MINUTES).build();
		Builder builder = new Request.Builder().url(url);
		builder.addHeader("Authorization", Credentials.basic(username, password)).post(body)
				.addHeader("content-type", "text/xml").addHeader("cache-control", "no-cache");
		if (!Objects.isNull(action)) {
			builder.addHeader("SOAPAction", action);
		}
		Request request = builder.build();
		Response response = client.newCall(request).execute();
		return response;
	}
}

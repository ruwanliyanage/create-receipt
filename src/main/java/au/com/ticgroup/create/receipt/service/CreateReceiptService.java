package au.com.ticgroup.create.receipt.service;

import java.io.IOException;
import java.util.Objects;

import javax.xml.bind.JAXBException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.ticgroup.create.receipt.config.HsConfig;
import au.com.ticgroup.create.receipt.dto.TggCrcReceiptItem;
import au.com.ticgroup.create.receipt.dto.TggRequestDto;
import au.com.ticgroup.create.receipt.util.HttpUtil;
import au.com.ticgroup.create.receipt.util.Util;
import au.com.ticgroup.soap.TicReceiptService.TGGCRCReceiptCasesType;
import au.com.ticgroup.soap.TicReceiptService.TGGCRCReceiptDesc;
import au.com.ticgroup.soap.TicReceiptService.TGGCRCReceiptItem;
import au.com.ticgroup.soap.TicReceiptService.TGGCRCReceiptResponse;
import au.com.ticgroup.soap.TicReceiptService.TicReceiptService;
import au.com.ticgroup.soap.TicReceiptService.TicReceiptService_Service;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;
import static au.com.ticgroup.create.receipt.util.HsConstants.*;

@Service
@Slf4j
public class CreateReceiptService {

	@Autowired
	private TicReceiptService_Service ticReceiptService;
	@Autowired
	protected ModelMapper modelMapper;
	@Autowired
	protected Util<Object> utilObj;
	@Autowired
	private HsConfig hsConfig;

	public String createTggReceipt(TggRequestDto body) throws JAXBException {
		log.info("Invoking CreateReceiptService :: createTggReceipt() :: body ::" + body);

		TGGCRCReceiptResponse resposneObject = null;
		String resultString = "";
		String errorMessage = "";
		try {

			TGGCRCReceiptDesc requestObject = modelMapper.map(body, TGGCRCReceiptDesc.class);

			for (TggCrcReceiptItem itemObject : body.getTggCrcReceiptItem()) {
				TGGCRCReceiptItem mappedItemObject = modelMapper.map(itemObject, TGGCRCReceiptItem.class);
				TGGCRCReceiptCasesType mappedCaseObject = new TGGCRCReceiptCasesType();

				if (Objects.nonNull(itemObject.getReceiptCase().getSolvupCase())) {
					for (String caseId : itemObject.getReceiptCase().getSolvupCase()) {
						mappedCaseObject.getSolvupCaseId().add(caseId);
						mappedItemObject.setTGGCRCReceiptCases(mappedCaseObject);
					}
					requestObject.getTGGCRCReceiptItem().add(mappedItemObject);
				}
			}

			TicReceiptService receiptServiceObject = ticReceiptService.getTicReceiptServiceSOAP();

			log.info("Invoking CreateReceiptService :: createReceipt() :: requestObject ::" + requestObject);
			resposneObject = receiptServiceObject.createReceipt(requestObject);
			log.info("Invoking CreateReceiptService :: createReceipt() :: resposneObject ::" + resposneObject);

		} catch (Exception e) {
			log.error("Exception :: CreateReceiptService() :: createReceipt " + e.getLocalizedMessage());
			errorMessage = e.getLocalizedMessage();
		}

		if (!resposneObject.getStatus().equalsIgnoreCase("success")) {
			resposneObject.setStatus("ERROR");
			resposneObject.setErrorCode("500");
			resposneObject.setErrorMessage(errorMessage);
		}

		resultString = utilObj.convertMessageToXmlString(resposneObject, Boolean.FALSE, "");
		String removeNs = " xmlns=\"http://thegoodguys.com.au/osb/rlo/tic/receipt\"";
		resultString = resultString.replace(removeNs, "").trim();

		return resultString;
	}

	public String createHsfReceipt(String xml) {
		log.info("Invoking CreateReceiptService :: createHsfReceipt() :: body :: " + xml);

		xml = xml.replaceAll("<Inventory ", "<nam:Inventory ");
		xml = xml.replaceAll("<Inventory>", "<nam:Inventory>");
		xml = xml.replaceAll("</Inventory>", "</nam:Inventory>");

		log.info("createHsfReceipt :: Service Url :: " + hsConfig.getLogisticsUrl() + " Service Action ::"
				+ hsConfig.getLogisticsAction());

		String response = createRequestAndSendToHs(xml, hsConfig.getLogisticsUrl(), hsConfig.getLogisticsAction());

		if (response.equals(HS_SOAP_BLANK_RESPONSE)) {
			response = TIC_SOAP_LOGISTICS_RESPONSE_OK;
		} else {
			response = "<Fault><faultcode>Server</faultcode><faultstring>General web service error : Invalid XML Response ? "
					+ response + "</faultstring></Fault>";
		}
		return response;
	}

	private String createRequestAndSendToHs(String xml, String url, String action) {

		log.info("Invoking CreateReceiptService :: createRequestAndSendToHs() :: xml :: " + xml + ":: url :: " + url
				+ ":: action :: " + action);

		String responseData = null;
		StringBuffer requestBuffer = new StringBuffer();
		requestBuffer.append(HS_SOAP_ENVELOPE_HSCLAIMSERVICE_START);
		requestBuffer.append(HS_SOAPHEADER_START);
		requestBuffer.append(HS_SECURITY_START);
		requestBuffer.append(HS_TOKEN_START);
		requestBuffer.append(hsConfig.getOutBoundSecurity());
		requestBuffer.append(HS_TOKEN_END);
		requestBuffer.append(HS_SECURITY_END);
		requestBuffer.append(HS_SOAPHEADER_END);
		requestBuffer.append(HS_SOAP_BODY_START);
		requestBuffer.append(xml);
		requestBuffer.append(HS_SOAP_BODY_END);
		requestBuffer.append(HS_SOAP_ENVELOPE_END);
		String finalXml = requestBuffer.toString();

		log.info("Invoking CreateReceiptService :: createRequestAndSendToHs() :: request xml :: " + finalXml);

		RequestBody body = RequestBody.create(MediaType.parse("text/xml"), finalXml);
		try {
			Response response = HttpUtil.makeXmlPostRequest(url, body, action, hsConfig.getUsername(),
					hsConfig.getPassword());
			responseData = new String(response.body().bytes());
			response.body().close();

			log.info("Response CreateReceiptService ::  createRequestAndSendToHs :: Response ::" + responseData);
		} catch (IOException e) {
			e.printStackTrace();
			responseData = e.getMessage();
		}
		return responseData;
	}

}

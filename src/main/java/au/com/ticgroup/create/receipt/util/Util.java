package au.com.ticgroup.create.receipt.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import au.com.ticgroup.create.receipt.transformers.TypeConvertor;

@Component
public class Util<T>{	

	public static Document getDocument(String body)throws ParserConfigurationException, UnsupportedEncodingException, SAXException, IOException{
		body = body.trim().replaceFirst("^([\\W]+)<", "<");
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource src = new InputSource();
		src.setCharacterStream(new StringReader(body));
		Document doc = builder.parse(src);
		return doc;
	}
	
	
	public String convertMessageToXmlString(T obj, Boolean isNoRootElmnt,String elementName){
		String xmlString = "";
		try{
			if(isNoRootElmnt){
				TypeConvertor<T> tcRtv = new TypeConvertor<>();
				xmlString = tcRtv.convertObjectToXmlWithoutRootElemt(obj,elementName);
			}else{
				TypeConvertor<T> tc = new TypeConvertor<>();
				xmlString = tc.convertObjectToXml(obj);
			}
		}catch(JAXBException e){
			e.printStackTrace();
		}
		return xmlString;
	}

}

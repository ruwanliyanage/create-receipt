package au.com.ticgroup.create.receipt.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "TGGCRCReceiptCases")
public class TggCrcReceiptCase {
	
	@XmlElement(name = "solvup_case_id")
	protected List<String>  solvupCase;

}

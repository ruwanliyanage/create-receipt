package au.com.ticgroup.create.receipt.transformers;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.springframework.stereotype.Component;

@Component
public class TypeConvertor<T> {
    
    public String convertObjectToXml(T t) throws JAXBException{
            JAXBContext context = JAXBContext.newInstance(t.getClass());
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            StringWriter sw = new StringWriter();
            m.marshal(t,sw);
            return sw.toString();
    }
    

	public String convertMessageToXmlString(T obj, Boolean isNoRootElmnt, String elementName){
		String xmlString = "";
		try{
			if(isNoRootElmnt){
				TypeConvertor<T> tcRtv = new TypeConvertor<>();
				xmlString = tcRtv.convertObjectToXmlWithoutRootElemt(obj,elementName);
			}else{
				TypeConvertor<T> tc = new TypeConvertor<>();
				xmlString = tc.convertObjectToXml(obj);
			}
		}catch(JAXBException e){
			e.printStackTrace();
		}
		return xmlString;
	}
    
	public  String convertObjectToXmlWithoutRootElemt(T t, String elementName) throws JAXBException {
  	  StringWriter stringWriter = new StringWriter();

  	  JAXBContext jaxbContext = JAXBContext.newInstance(t.getClass());
  	  Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
  	  jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT,  Boolean.TRUE);

  	  QName qName = new QName("au.com.ticgroup."+elementName, elementName);
  	  @SuppressWarnings("unchecked")
		  JAXBElement<T> root = new JAXBElement<T>(qName,(Class<T>) t.getClass(),t);
  	  jaxbMarshaller.marshal(root, stringWriter);

  	  String result = stringWriter.toString();
  	  result = result.replaceAll("ns2:","");
  	  return result;
  	}  
    
     
}

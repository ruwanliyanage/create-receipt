
package au.com.ticgroup.soap.TicReceiptService;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transfer_number" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}number12"/>
 *         &lt;element name="store_id" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}number12"/>
 *         &lt;element name="source_id" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}number12"/>
 *         &lt;element name="user" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}varchar2128"/>
 *         &lt;element name="receipt_date" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="status" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}varchar212"/>
 *         &lt;element ref="{http://thegoodguys.com.au/osb/rlo/tic/receipt}TGGCRCReceiptItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferNumber",
    "storeId",
    "sourceId",
    "user",
    "receiptDate",
    "status",
    "tggcrcReceiptItem"
})
@XmlRootElement(name = "TGGCRCReceiptDesc")
public class TGGCRCReceiptDesc {

    @XmlElement(name = "transfer_number")
    protected long transferNumber;
    @XmlElement(name = "store_id")
    protected long storeId;
    @XmlElement(name = "source_id")
    protected long sourceId;
    @XmlElement(required = true)
    protected String user;
    @XmlElement(name = "receipt_date", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar receiptDate;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(name = "TGGCRCReceiptItem")
    protected List<TGGCRCReceiptItem> tggcrcReceiptItem;

    /**
     * Gets the value of the transferNumber property.
     * 
     */
    public long getTransferNumber() {
        return transferNumber;
    }

    /**
     * Sets the value of the transferNumber property.
     * 
     */
    public void setTransferNumber(long value) {
        this.transferNumber = value;
    }

    /**
     * Gets the value of the storeId property.
     * 
     */
    public long getStoreId() {
        return storeId;
    }

    /**
     * Sets the value of the storeId property.
     * 
     */
    public void setStoreId(long value) {
        this.storeId = value;
    }

    /**
     * Gets the value of the sourceId property.
     * 
     */
    public long getSourceId() {
        return sourceId;
    }

    /**
     * Sets the value of the sourceId property.
     * 
     */
    public void setSourceId(long value) {
        this.sourceId = value;
    }

    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUser(String value) {
        this.user = value;
    }

    /**
     * Gets the value of the receiptDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReceiptDate() {
        return receiptDate;
    }

    /**
     * Sets the value of the receiptDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReceiptDate(XMLGregorianCalendar value) {
        this.receiptDate = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the tggcrcReceiptItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tggcrcReceiptItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTGGCRCReceiptItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TGGCRCReceiptItem }
     * 
     * 
     */
    public List<TGGCRCReceiptItem> getTGGCRCReceiptItem() {
        if (tggcrcReceiptItem == null) {
            tggcrcReceiptItem = new ArrayList<TGGCRCReceiptItem>();
        }
        return this.tggcrcReceiptItem;
    }

}


package au.com.ticgroup.soap.TicReceiptService;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the au.com.ticgroup.soap.TicReceiptService package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: au.com.ticgroup.soap.TicReceiptService
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TGGCRCReceiptItem }
     * 
     */
    public TGGCRCReceiptItem createTGGCRCReceiptItem() {
        return new TGGCRCReceiptItem();
    }

    /**
     * Create an instance of {@link TGGCRCReceiptDesc }
     * 
     */
    public TGGCRCReceiptDesc createTGGCRCReceiptDesc() {
        return new TGGCRCReceiptDesc();
    }

    /**
     * Create an instance of {@link TGGCRCReceiptCasesType }
     * 
     */
    public TGGCRCReceiptCasesType createTGGCRCReceiptCasesType() {
        return new TGGCRCReceiptCasesType();
    }

    /**
     * Create an instance of {@link TGGCRCReceiptResponse }
     * 
     */
    public TGGCRCReceiptResponse createTGGCRCReceiptResponse() {
        return new TGGCRCReceiptResponse();
    }

}

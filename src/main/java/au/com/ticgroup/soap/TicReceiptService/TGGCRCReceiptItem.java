
package au.com.ticgroup.soap.TicReceiptService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sku" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}number12"/>
 *         &lt;element name="barcode" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}varchar225"/>
 *         &lt;element name="quantity" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}number12"/>
 *         &lt;element name="credit_qty" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}number12"/>
 *         &lt;element name="damaged_qty" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}number12"/>
 *         &lt;element name="TGGCRCReceiptCases" type="{http://thegoodguys.com.au/osb/rlo/tic/receipt}TGGCRCReceiptCasesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sku",
    "barcode",
    "quantity",
    "creditQty",
    "damagedQty",
    "tggcrcReceiptCases"
})
@XmlRootElement(name = "TGGCRCReceiptItem")
public class TGGCRCReceiptItem {

    protected long sku;
    @XmlElement(required = true)
    protected String barcode;
    protected long quantity;
    @XmlElement(name = "credit_qty")
    protected long creditQty;
    @XmlElement(name = "damaged_qty")
    protected long damagedQty;
    @XmlElement(name = "TGGCRCReceiptCases")
    protected TGGCRCReceiptCasesType tggcrcReceiptCases;

    /**
     * Gets the value of the sku property.
     * 
     */
    public long getSku() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     */
    public void setSku(long value) {
        this.sku = value;
    }

    /**
     * Gets the value of the barcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * Sets the value of the barcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarcode(String value) {
        this.barcode = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     */
    public long getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     */
    public void setQuantity(long value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the creditQty property.
     * 
     */
    public long getCreditQty() {
        return creditQty;
    }

    /**
     * Sets the value of the creditQty property.
     * 
     */
    public void setCreditQty(long value) {
        this.creditQty = value;
    }

    /**
     * Gets the value of the damagedQty property.
     * 
     */
    public long getDamagedQty() {
        return damagedQty;
    }

    /**
     * Sets the value of the damagedQty property.
     * 
     */
    public void setDamagedQty(long value) {
        this.damagedQty = value;
    }

    /**
     * Gets the value of the tggcrcReceiptCases property.
     * 
     * @return
     *     possible object is
     *     {@link TGGCRCReceiptCasesType }
     *     
     */
    public TGGCRCReceiptCasesType getTGGCRCReceiptCases() {
        return tggcrcReceiptCases;
    }

    /**
     * Sets the value of the tggcrcReceiptCases property.
     * 
     * @param value
     *     allowed object is
     *     {@link TGGCRCReceiptCasesType }
     *     
     */
    public void setTGGCRCReceiptCases(TGGCRCReceiptCasesType value) {
        this.tggcrcReceiptCases = value;
    }

}
